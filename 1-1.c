//Antonio Silva Paucar CIS 314

#include <stdio.h>
void printBytes(unsigned char *start, int len) {
	for (int i = 0; i < len; ++i) {
		printf(" %.2x", start[i]);
	}
	printf("\n");
}
void printInt(int x) {
	printBytes((unsigned char *) &x, sizeof(int));
}
void printFloat(float x) {
	printBytes((unsigned char *) &x, sizeof(float));
}
void printShort(short x) {
	printBytes((unsigned char *) &x, sizeof(short));
}
void printLong(long x) {
	printBytes((unsigned char *) &x, sizeof(long));
}
void printDouble(double x) {
	printBytes((unsigned char *) &x, sizeof(double));
}

int main()
{

    int sample = 7;
	printf("Int: ");
    printInt(sample);
	printf("Float: ");
    printFloat(sample);
	printf("Short: ");
    printShort(sample);
	printf("Long: ");
    printLong(sample);
	printf("Double: ");
    printDouble(sample);
	
	printf("\n");
	
	sample = 11;
	printf("Int: ");
    printInt(sample);
	printf("Float: ");
    printFloat(sample);
	printf("Short: ");
    printShort(sample);
	printf("Long: ");
    printLong(sample);
	printf("Double: ");
    printDouble(sample);
	
	printf("\n");

	float sample1 = 10.223;
	printf("Int: ");
    printInt(sample1);
	printf("Float: ");
    printFloat(sample1);
	printf("Short: ");
    printShort(sample1);
	printf("Long: ");
    printLong(sample1);
	printf("Double: ");
    printDouble(sample1);
	
	printf("\n");

	
	//Something unexpected I can see in the output of this code is that
	//the direction from where the data is stored seems to come from left to right
	//in some cases and from right to left in others.
	
	// We find also that there is a difference how the same value is expressed in different types. The INT and the DOUBLE
	// can be the same value but the prints show us different patterns. The book mention that floats use a different coding scheme]
	// and that is the main reason of that.
	
	//I tried the code in Windows and Mac. I found out that 
	//while long type just print 4 pairs of zeros in Windows,
	//On Mac, it prints 8 pairs of zeros, meaning that it uses
	//more bytes to store Long types and this is according to the text
	// that indicates that different system uses different number of bytes for storage.
	//(both system were x64)
	

   return 0;
}
