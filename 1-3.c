//Antonio Silva Paucar
//I turned the files in at 11:52 pm of thursday. I just realized this file had a error due my comments. I corrected it.

#include <stdio.h>

int replace(unsigned first, unsigned char second, int location)
{
	//train of thought.

        //int shift= location>>3;// moved


	//12       34       56       78
	//00001100 00100010 00111000 01001110      x =12345678
	//00000000 00000000 00000000 11111111 << 0 mascara
        //11111111 11111111 11111111 00000000    ¬ mascara
	//----------------------------------- an x&¬mascara
	//12       34       56       00
	//00001100 00100010 00111000 00000000      x&¬mascara  =12345600
	//00000000 00000000 00000000 10101011  b  case 0xAB
	//----------------------------------- or
	//12       34       56       AB
	//00001100 00100010 00111000 10101011  |  answer!

	//int replace = (x & ~mascara) | (b << location);


	//unsigned mascara = 0x000000FF << (location<<3); // location move from 0 to 8, 16,24

	 return (first & ~(0x000000FF << (location<<3))) | (second << (location<<3)); //the location variable is used to move the mask and isolate
											// the desire bit. then we fusion the two values after ta
}

int main(void)
{

	int a  =  replace(0x12345678,0xAB,3);
	printf("%#010x\n",a);
	int b = replace(0x12345678,0xAB,0);
	printf("%#010x\n",b);

}
