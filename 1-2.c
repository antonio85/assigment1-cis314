#include <stdio.h>

int combine(unsigned first, unsigned second)
{
	
	//78       56       34       12       
	//01001110 00111000 00100010 00001100      x =12345678
	//00000000 00000000 11111111 11111111  mascara x
	//-----------------------------------  & and
        //00000000 00000000 00100010 00001100   resultado 1    



	
	//00000000 11101111 11001101 10101011    x =ABCDEF00
	//11111111 11111111 00000000 00000000 mascara y
	//----------------------------------- & and
	//00000000 11101111 00000000 00000000  resultado 2



        //00000000 00000000 00100010 00001100  resultado 1
	//00000000 11101111 00000000 00000000  resultado 2
	//-----------------------------------| or
	//00000000 11101111 00100010 00001100
	
	//unsigned mascara1 = 0xFFFF0000; // fir   
	//unsigned mascara2 = 0x0000FFFF;

	 return (first & 0xFFFF0000) | (second & 0x0000FFFF); //it takes the first value and use the mask to isolate the first 2 terms with the "and" operator, then we do the same with the second term
							      //but in this case, we isolate the third and the fourth. Finally, we mix the two previous results with the "or" operator to obtain the 
						              // the answer, a value with the first four numbers from one value and the last 4 from the other.
}

int main(void)
{
	int a  =  combine(0x12345678, 0xABCDEF00);
	printf("%#010x\n",a);
	int b = combine(0xABCDEF00, 0x12345678);
	printf("%#010x\n",b);
}
